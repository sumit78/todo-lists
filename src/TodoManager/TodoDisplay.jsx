import React from 'react';
import TodoList from './TodoList';
import './style.css';

class TodoDisplay extends React.Component{
  constructor(props){
    super(props)
    this.state={
      inputname:"",
      items:[],
      isDisplay: false
    }
  }

  handlerChange = (event) => {
    this.setState({
      inputname:event.target.value
    });
  
  }

  todoHandlerClick = (e) => {
  if(this.state.inputname === ""){
    alert("please enter the task first")
    }else{
      this.state.items.push(this.state.inputname);
      this.setState({
        isDisplay: true
      }); 
    }  
  }

  deleteItem = (selectedItem) => {
    const filteredItems= this.state.items.filter(item =>{
      console.log(item,selectedItem)
      return item !== selectedItem;
     
    })
    console.log("fi",filteredItems)
    this.setState({
      items:filteredItems,
    })
  }

  

  render(){
    return(
      <div className="container">
        <input  type="text" value={this.state.name} onChange={this.handlerChange.bind(this)} onKeyPress={this.handlerChange.bind(this)}/>
        <button type="button" onClick={this.todoHandlerClick.bind(this)}> AddTask</button>  
     <br/>
      <h4>Tasks </h4>
      {this.state.isDisplay &&
        <TodoList    lists = {this.state.items} deleteItem = {this.deleteItem} ></TodoList>
      }
      </div>
    )
  }
}
export default TodoDisplay;