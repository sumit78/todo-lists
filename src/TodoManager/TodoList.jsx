import React from 'react';

class TodoList extends React.Component{
  render(){
    const todolist = this.props.lists;
    const listItems= todolist.map( (item,index) => {
       return <li  key={index} onClick={()=>this.props.deleteItem(item)}>{item}</li>
    }); 
  return(
   <div>
     <ul>{listItems}</ul>
     </div>
  )  
}
}

export default TodoList;